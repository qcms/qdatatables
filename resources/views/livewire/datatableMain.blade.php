<div class="-my-2 w-full p-4">
    <div class="py-2 align-middle inline-block sm:px-8 lg:px-11 w-full">
        <div class="shadow overflow-hidden border-b border-gray-200 bg-white p-2 sm:rounded-lg w-full">
            @livewire('qcms::datatable-search')
            <div class="m-5">
                {{ $resource->links() }}
            </div>
{{--            @include('qcms::livewire.partials.datatableTable')--}}
            <div class="m-5">
{{--                {{ $resources->links() }}--}}
            </div>
        </div>
    </div>
</div>

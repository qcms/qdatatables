<?php


namespace Qcms\Qdatatable\Livewire;


use Livewire\Component;
use Qcms\Qdatatable\Facades\Qdatatable;

class DatatableSearch extends Component
{
    public function render()
    {
        return view('qcms::livewire.datatableSearch');
    }
}

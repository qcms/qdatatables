<?php


namespace Qcms\Qdatatable\Livewire;


use Livewire\Component;

class DatatableFilter extends Component
{
    public function render()
    {
        return view('qcms::livewire.datatableFilter');
    }
}

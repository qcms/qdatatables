<?php


namespace Qcms\Qdatatable\Livewire;


use Livewire\Component;
use Qcms\Qdatatable\Facades\Qdatatable;

class DatatableBase extends Component
{
    public $resource;

    /**
     * Mounts the Livewire component and registers the Datatable object
     *
     * @param Qdatatable $resource
     */
    public function mount(Qdatatable $resource) {
        $this->resource = $resource;
    }
    public function render()
    {
        return view('qcms::livewire.datatableMain',[
            'resource' => $this->resource
        ]);
    }

    private function parameters()
    {
        return [
            'search' => $this->search,
            'email' => $this->email,
            'name' => $this->name
        ];
    }

}

<?php

namespace Qcms\Qdatatable;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Qcms\Qdatatable\Livewire\DatatableBase;
use Qcms\Qdatatable\Livewire\DatatableSearch;

class QdatatableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('qdatatable', Qdatatable::class);
        Livewire::component('qcms::datatable-base', DatatableBase::class);
        Livewire::component('qcms::datatable-search', DatatableSearch::class);
        Livewire::component('qcms::datatable-body', DatatableBase::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'qcms');
    }
}

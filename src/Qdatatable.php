<?php


namespace Qcms\Qdatatable;


use Qcms\Qdatatable\Models\Datatable;

class Qdatatable
{
    public $search;

    /**
     * @param $model
     * @return Datatable
     */
    public function datatable($model) {
       return new Datatable($model);
    }
}

<?php


namespace Qcms\Qdatatable\Traits;


use Qcms\Qdatatable\Models\Datatable;

trait hasDatatable
{
    public static function getCollection(){
        return new Datatable(self::class);
    }
}

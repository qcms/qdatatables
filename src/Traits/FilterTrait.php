<?php


namespace Qcms\Qdatatable\Traits;


use Qcms\Qdatatable\Models\FilterBuilder;
use Qcms\Qdatatable\Models\FilterNames;

Trait FilterTrait
{
    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function scopeFilterBy($query, $filters)
    {
        $namespace = self::class;
        $filter = new FilterBuilder($query, $filters, $namespace);

        return $filter->apply();
    }

    public static function filterNames() {
        return (new FilterNames())->parameters()->get(self::class);
    }
}

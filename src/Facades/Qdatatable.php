<?php


namespace Qcms\Qdatatable\Facades;


use Illuminate\Support\Facades\Facade;

class Qdatatable extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'qdatatable';
    }
}

<?php


namespace Qcms\Qdatatable\Models;


/**
 * Interface FilterContract
 * @package App\Tools
 */
interface FilterContract
{
    public function handle($value): void;
}

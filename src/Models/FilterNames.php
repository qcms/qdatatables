<?php


namespace Qcms\Qdatatable\Models;


use Illuminate\Support\Collection;

class FilterNames
{
    private $filterLocation;

    public $filters = [];
    /**
     * @return Collection
     */
    public function parameters(): Collection
    {
        return collect([$this->filterLocation => $this->filters]);
    }

    public function addModel(String $model)
    {
        $this->filterLocation =  "App\Filters" . $model;
    }

    public function addFilter($name, $value, $output = 'string')
    {
        $this->filters[$value] = [
            'text' => $name, // Displays the name of the filter
            'value' => $value, // Fills the correct parameter
            'output' => $output // Helps with type of filter
        ];
    }
}

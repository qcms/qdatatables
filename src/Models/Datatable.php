<?php


namespace Qcms\Qdatatable\Models;


use Illuminate\Support\Collection;

class Datatable
{
    private $collection;

    public function __construct(String $model)
    {
        $this->collection = new Collection($model);
    }

    /**
     * @return Collection
     */
    public function getCollection(): Collection
    {
        return $this->collection;
    }

    public function render(String $customLivewireComponent = null) {
        return view('qcms::livewire.datatableMain');
    }
}

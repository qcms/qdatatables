<?php


namespace Qcms\Qdatatable\Models;


/**
 * Class FilterBuilder
 * Handles filter and search values of the client
 *
 * @package App\Tools
 */
class FilterBuilder
{
    protected $query;
    protected $filter;
    protected $namespace;

    /**
     * FilterBuilder constructor.
     *
     * @param $query
     * @param $filters
     * @param $namespace
     */
    public function __construct($query, $filters, $namespace)
    {
        $this->query = $query;
        $this->filter = $filters;
        $this->namespace = $namespace;
    }

    /**
     * @return mixed
     */
    public function apply()
    {
        foreach ($this->filter as $name => $val) {
            $normalizedName = ucfirst($name);
            $class = $this->namespace . "\\{$normalizedName}";
            if (! class_exists($class)) {
                continue;
            }

            if (strlen($val)) {
                (new $class($this->query))->handle($val);
            }
        }

        return $this->query;
    }
}
